import React, { Component } from 'react'
import './App.scss'
import { Header, Content } from 'Components'
import { Provider } from 'react-redux'
import { store } from './CommonLogic/store'

class App extends Component {
  render () {
    return (
      <Provider store={store}>
        <div className='App'>
          <Header />
          <Content />
        </div>
      </Provider>
    )
  }
}

export default App
