import { combineReducers } from 'redux'
import { reducer as reduxFormReducer } from 'redux-form'

const baseReducer = combineReducers({
  form: reduxFormReducer
})

export default baseReducer
