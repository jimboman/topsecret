import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Link } from '@reach/router'
import './Sidebar.scss'
import svgSprite from 'img/sprite.svg'
import { Toggle, InfoModal } from 'Components/Modals'

class NavLink extends PureComponent {
  render () {
    return (
      <Link
        {...this.props}
        getProps={({ isCurrent }) => isCurrent &&
         { className: `${this.props.className} isActive` }}
      />
    )
  }
}

class Sidebar extends PureComponent {
  isDvkChecked = ({ isProportion, isMajor, isRajon, isOblRada, isMiskRada, isMiskGolova, isSelskRada, isSelskStarosta }) => (
    isProportion || isMajor || isRajon || isOblRada || isMiskRada || isMiskGolova || isSelskRada || isSelskStarosta
  )

  render () {
    const { dvkFormValues } = this.props
    return (
      <nav className='sidebar'>
        <ul className='side-nav'>
          <li className='side-nav__item'>
            <NavLink className='side-nav__link' to='dvk'>
              <svg className='side-nav__icon'>
                <use xlinkHref={`${svgSprite}#icon-tools`} />
              </svg>
              <span>
            Налаштування даних ДВК
              </span>
            </NavLink>
          </li>
          <li className='side-nav__item'>
            <NavLink className='side-nav__link' to='names'>
              <svg className='side-nav__icon'>
                <use xlinkHref={`${svgSprite}#icon-users`} />
              </svg>
              <span>
            Налаштування даних партій/кандидатів
              </span>
            </NavLink>
          </li>
          {
            this.isDvkChecked(dvkFormValues) &&
            <>
              <li className='side-nav__item'>
                <NavLink className='side-nav__link' to='protocol'>
                  <svg className='side-nav__icon'>
                    <use xlinkHref={`${svgSprite}#icon-menu`} />
                  </svg>
                  <span>
                  Протоколи про підрахунок голосів виборців
                  </span>
                </NavLink>
              </li>
              <li className='side-nav__item'>
                <NavLink className='side-nav__link' to='law'>
                  <svg className='side-nav__icon'>
                    <use xlinkHref={`${svgSprite}#icon-open-book`} />
                  </svg>
                  <span>
                    Законодавство та нормативні акти
                  </span>
                </NavLink>
              </li>
              </>
          }
          <li className='side-nav__item'>
            <Toggle>
              {({ on, toggle }) => (
                <>
                  <span onClick={toggle} className='side-nav__link' >
                    <svg className='side-nav__icon'>
                      <use xlinkHref={`${svgSprite}#icon-info`} />
                    </svg>
                    <span>Про програму</span>
                  </span>
                  <InfoModal on={on} toggle={toggle}>
                    <div>
                    Довідка
Дана програма є методичним матеріалом, призначеним для контролю правильності заповнення протоколу дільничної виборчої комісії про підрахунок голосів виборців на виборчій дільниці на виборах народних депутатів України, а також на місцевих виборах.

                    </div>
                  </InfoModal>
                </>
              )}
            </Toggle>
            {/* <NavLink className='side-nav__link' to='info'> */}
            {/* <svg className='side-nav__icon'>
                <use xlinkHref={`${svgSprite}#icon-info`} />
              </svg> */}
            {/* </NavLink> */}
          </li>
        </ul>
      </nav>
    )
  }
}

Sidebar.propTypes = {
  dvkFormValues: PropTypes.object
}

const mapStateToProps = state => {
  return { dvkFormValues: state.form.dvkForm ? state.form.dvkForm.values : {} }
}
export default connect(mapStateToProps)(Sidebar)
