import React, { Component } from 'react'
import Dashboard from './Dashboard'
import Sidebar from './Sidebar'
import Header from './Header'
import './Content.scss'

class Content extends Component {
  render () {
    return (
      <div className='content'>
        <Sidebar />
        <Dashboard />
      </div>
    )
  }
}

export {
  Content,
  Header
}
