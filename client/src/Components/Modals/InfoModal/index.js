import React, { Component } from 'react'
import Portal from '../Portal'
import PropTypes from 'prop-types'
import './InfoModal.scss'

export default class InfoModal extends Component {
  render () {
    const { children, toggle, on } = this.props
    return (
      <Portal>
        {on && (
          <div className='modal' onClick={toggle}>
            <div className='modal__card'>
              <button onClick={toggle} className='modal__card-closeBtn btn'> Close</button>
              <div>
                {children}
              </div>
            </div>
          </div>
        )}
      </Portal>
    )
  }
}

InfoModal.propTypes = {
  children: PropTypes.node,
  toggle: PropTypes.func,
  on: PropTypes.bool
}
