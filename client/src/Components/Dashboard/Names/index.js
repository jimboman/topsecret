import React from 'react'
import PropTypes from 'prop-types'
import { Field, FieldArray, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import './Names.scss'
import svgSprite from 'img/sprite.svg'

const TITLES = {
  rajonRada:
    'Перелік місцевих організацій політичних партій, які висунули виборчі списки кандидатів у депутати районної ради',
  mistRada:
    'Перелік місцевих організацій політичних партій, які висунули виборчі списки кандидатів у депутати районної в місті ради',
  miskRada:
    'Перелік місцевих організацій політичних партій, які висунули виборчі списки кандидатів у депутати міської ради',
  major: 'Перелік кандидатів в депутати по одномандатному виборчому округу',
  proportion:
    'Перелік політичних партій, які висунули виборчі списки кандидатів у депутати від політичних партій у загальнодержавному багатомандатному окрузі з виборів народних депутатів України',
  oblRada:
    'Перелік місцевих організацій політичних партій, які висунули виборчі списки кандидатів у депутати обласної ради',
  krim:
    'Перелік місцевих організацій політичних партій, які висунули виборчі списки кандидатів у депутати Верховної Ради Автономної Республіки Крим',
  miskGolova: 'Перелік кандидатів на посаду міського голови',
  selishGolova: 'Перелік кандидатів на посаду селищного голови',
  silskGolova: 'Перелік кандидатів на посаду сільського голови',
  selskRada: 'Перелік кандидатів в депутати сільської ради',
  selishRada: 'Перелік кандидатів в депутати селищної ради',
  selskStarosta: 'Перелік кандидатів на посаду сільського старости',
  selishStarosta: 'Перелік кандидатів на посаду селищного старости'
}

const renderField = ({ input, className, type, meta: { touched, error } }) => (
  <>
    <input className={className} {...input} type={type} />
    {touched && error && <span>{error}</span>}
  </>
)

const renderNames = ({
  fields,
  title,
  columnTitle,
  btnTitle,
  meta: { error }
}) => (
  <>
    <table className='names__table'>
      <caption>{title}</caption>
      <thead>
        <tr>
          <th className='firstCol'>№</th>
          <th className='secondCol'>{columnTitle}</th>
        </tr>
      </thead>
      <tbody>
        {fields.map((name, index) => (
          <tr key={index}>
            <td data-label='№'>{index + 1}</td>
            <td data-label='Назва політичної партії'>
              <div>
                <Field
                  name={name}
                  type='text'
                  component={renderField}
                  className='names__table-input'
                />
                <svg
                  className='remove-btn__icon'
                  title='Видалити скриньку'
                  onClick={() => fields.remove(index)}
                >
                  <use xlinkHref={`${svgSprite}#icon-circle-with-cross`} />
                </svg>
              </div>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
    <button className='btn' onClick={() => fields.push()}>
      {btnTitle}
    </button>
  </>
)
const NameForm = ({ name, type }) => (
  <FieldArray
    name={name}
    component={renderNames}
    title={TITLES[`${name}`]}
    columnTitle={
      type === 'names'
        ? `Прізвище, власне ім’я та по батькові кандидата`
        : 'Назва політичної партії'
    }
    btnTitle={type === 'names' ? `Додати кандидата` : 'Додати партiю'}
  />
)
let NameArraysForm = props => {
  const {
    isProportion,
    isMajor,
    isOblRada,
    isRajon,
    isMiskRada,
    isMiskGolova,
    isSelskRada,
    isSelskStarosta,
    rajonRada,
    oblRada,
    miskGolova,
    selskRada,
    selskStarosta
  } = props.dvkFormValues
  return (
    <div className='names'>
      <h1 className='heading-1 mb-lg'>Налаштування даних партій/кандидатів</h1>
      {isProportion && <NameForm name='proportion' />}
      {isMajor && <NameForm name='major' />}
      {isOblRada && <NameForm name={oblRada} />}
      {isRajon && <NameForm name={rajonRada} />}
      {isMiskRada && <NameForm name='miskRada' />}
      {isMiskGolova && <NameForm name={miskGolova} type='names' />}
      {isSelskRada && <NameForm name={selskRada} type='names' />}
      {isSelskStarosta && <NameForm name={selskStarosta} type='names' />}
    </div>
  )
}

NameForm.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.sting
}

NameArraysForm.propTypes = {
  dvkFormValues: PropTypes.object
}

NameArraysForm = reduxForm({
  form: 'names',
  initialValues: {
    miskRada: [null, null],
    rajonRada: [null, null],
    proportion: [null, null],
    major: [null, null]
  },
  destroyOnUnmount: false
})(NameArraysForm)

const mapStateToProps = state => {
  return { dvkFormValues: state.form.dvkForm ? state.form.dvkForm.values : {} }
}
export default connect(mapStateToProps)(NameArraysForm)
