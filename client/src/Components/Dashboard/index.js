import React, { Component } from 'react'
import Info from './Info'
import Law from './Law'
import Settings from './Settings'
import Protocol from './Protocol'
import Names from './Names'
import { BoxProtocolForm,
  CommonProtocolForm, withFormName } from 'Components/Dashboard/Protocol/ProtocolForms'
import './Dashboard.scss'
import { Router } from '@reach/router'
import { COMMONFORM, LINKNAMES, BOXPROTOCOLFORM } from 'Components/Dashboard/Protocol/textToFroms'

// const Proportion
// const Major
const Proportion = withFormName(BoxProtocolForm, 'proportion')
const Major = withFormName(BoxProtocolForm, 'major')
const Krim = withFormName(CommonProtocolForm, 'krim')
const OblRada = withFormName(CommonProtocolForm, 'oblRada')
const RajonRada = withFormName(CommonProtocolForm, 'rajonRada')
const MistRada = withFormName(CommonProtocolForm, 'mistRada')
const MiskRada = withFormName(CommonProtocolForm, 'miskRada')
const MiskGolova = withFormName(CommonProtocolForm, 'miskGolova')
const SelishGolova = withFormName(CommonProtocolForm, 'selishGolova')
const SelskGolova = withFormName(CommonProtocolForm, 'selskGolova')
const SelskRada = withFormName(CommonProtocolForm, 'selskRada')
const SelishRada = withFormName(CommonProtocolForm, 'selishRada')
const SelskStarosta = withFormName(CommonProtocolForm, 'selskStarosta')
const SelishStarosta = withFormName(CommonProtocolForm, 'selishStarosta')

// krim > oblRada > rajonRada > mistRada > miskRada > miskGolova > selishGolova
// selskGolova > selskRada > selishRada > selskStarosta > selishStarosta
export default class Dashboard extends Component {
  render () {
    return (
      <main className='dashboard' >
        <Router>
          <Settings path='/dvk' />
          <Names path='/names' />
          <Protocol path='/protocol' />
          <Proportion
            path='/protocol/proportion'
            boxProtocolText={BOXPROTOCOLFORM}
            header={LINKNAMES.proportion}
          />
          <Major
            path='/protocol/major'
            boxProtocolText={BOXPROTOCOLFORM}
            header={LINKNAMES.proportion}
          />
          <Krim
            path='/protocol/oblRada/krim'
            header={LINKNAMES.oblRada.krim}
            commonProtocolText={COMMONFORM}
          />
          <OblRada
            path='/protocol/oblRada/oblRada'
            header={LINKNAMES.oblRada.oblRada}
            commonProtocolText={COMMONFORM}
          />
          <RajonRada
            path='/protocol/rajon/rajonRada'
            header={LINKNAMES.rajon.rajonRada}
            commonProtocolText={COMMONFORM}
          />
          <MistRada
            path='/protocol/rajon/mistRada'
            header={LINKNAMES.rajon.mistRada}
            commonProtocolText={COMMONFORM}
          />
          <MiskRada
            path='/protocol/miskRada'
            header={LINKNAMES.miskRada}
            commonProtocolText={COMMONFORM}
          />
          <MiskGolova
            path='/protocol/miskGolova/miskGolova'
            header={LINKNAMES.miskGolova.miskGolova}
            commonProtocolText={COMMONFORM}
          />
          <SelishGolova
            path='/protocol/miskGolova/selishGolova'
            header={LINKNAMES.miskGolova.selishGolova}
            commonProtocolText={COMMONFORM}
          />
          <SelskGolova
            path='/protocol/miskGolova/selskGolova'
            header={LINKNAMES.miskGolova.selskGolova}
            commonProtocolText={COMMONFORM}
          />
          <SelskRada
            path='/protocol/selskRada/selskRada'
            header={LINKNAMES.selskRada.selskRada}
            commonProtocolText={COMMONFORM}
          />
          <SelishRada
            path='/protocol/selskRada/selishRada'
            header={LINKNAMES.selskRada.selishRada}
            commonProtocolText={COMMONFORM}
          />
          <SelskStarosta
            path='/protocol/selskStarosta/selskStarosta'
            header={LINKNAMES.selskStarosta.selskStarosta}
            commonProtocolText={COMMONFORM}
          />
          <SelishStarosta
            path='/protocol/selskStarosta/selishStarosta'
            header={LINKNAMES.selskStarosta.selishStarosta}
            commonProtocolText={COMMONFORM}
          />
          <Law path='/law' />
          <Info path='/info' />
        </Router>
      </main>
    )
  }
}
