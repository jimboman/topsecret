import React from 'react'
import { Field, FieldArray, reduxForm } from 'redux-form'
import './BoxForm.scss'
import svgSprite from 'img/sprite.svg'

const renderField = ({ input, label, type, meta: { touched, error } }) => (
  <>
    <input {...input} type={type} />
    {touched && error && <span>{error}</span>}
  </>
)

const renderBoxes = ({ fields }) => (
  <>
    <table className='table'>
      <thead>
        <tr>
          <th className='firstCol'>№</th>
          <th className='secondCol'>Тип скриньки</th>
          <th className='secondCol'>Номер скриньки</th>
          <th className='secondCol'>Видалити</th>

        </tr>
      </thead>
      <tbody>
        {fields.map((box, index) => {
          const memberObj = fields.get(index)
          return memberObj.type === 'stac'
            ? <tr key={index}>
              <td data-label='№'>{index + 1}</td>
              <td data-label='Тип скриньки'>
                <label>Стаціонарна скринька</label>
              </td>

              <td data-label='Номер скриньки'>
                <Field
                  name={`${box}.num`}
                  type='number'
                  component={renderField}
                />
              </td>
              <td data-label='Видалити'>
                <svg
                  className='remove-btn__icon'
                  title='Видалити скриньку'
                  onClick={() => fields.remove(index)}
                >
                  <use xlinkHref={`${svgSprite}#icon-circle-with-cross`} />
                </svg>
              </td>
            </tr>
            : <tr key={index}>
              <td data-label='№'>{index + 1}</td>
              <td data-label='Тип скриньки'>
                <label>Переносна скринька</label>
              </td>
              <td data-label='Номер скриньки'>
                <Field
                  name={`${box}.num`}
                  type='number'
                  component={renderField}
                />
              </td>
              <td data-label='Видалити'>
                <svg
                  className='remove-btn__icon'
                  title='Видалити скриньку'
                  onClick={() => fields.remove(index)}
                >
                  <use xlinkHref={`${svgSprite}#icon-circle-with-cross`} />
                </svg>
              </td>
            </tr>
        })}
      </tbody>
    </table>
    <button className='btn' onClick={() => fields.push({ type: 'stac' })}>+ Стаціонарна скринька</button>
    <button className='btn' onClick={() => fields.push({ type: 'perenos' })}>+ Переносна скринька</button>
  </>
)

const BoxForm = () => (
  <FieldArray name='box' component={renderBoxes} />
)

export default reduxForm({
  form: 'boxArrays',
  initialValues: {
    box: [{ type: 'stac' }, { type: 'perenos' }]
  },
  destroyOnUnmount: false
})(BoxForm)
