const equalLength = equal => value => value && value.length !== equal ? `
  Кількість цифр має бути рівнa ${equal}` : undefined

const minValue = min => value =>
  value && value < min ? `Должно быть хотябы ${min}` : undefined

const maxValue = max => value =>
  value && value > max ? `Не больше чем ${max}` : undefined

export const equalLength6 = equalLength(6)
export const minValue4 = minValue(4)
export const maxValue18 = maxValue(18)
