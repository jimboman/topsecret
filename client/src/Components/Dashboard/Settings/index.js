import React from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm, formValueSelector } from 'redux-form'
import BoxForm from './BoxForm'
import PropTypes from 'prop-types'
import './Settings.scss'
import { equalLength6, minValue4, maxValue18 } from './validation'

const renderField = ({
  input,
  type,
  title,
  className,
  meta: { touched, error, warning }
}) => (
  <div>
    <input {...input} className={className} type={type} title={title} />
    <div className='requirements'>
      {touched &&
        ((error && error) ||
          (warning && warning))}
    </div>
  </div>
)

let Settings = props => {
  const {
    handleSubmit,
    isProportionValue,
    isMajorValue,
    isOblRadaValue,
    isRajonValue,
    isMiskRadaValue,
    isMiskGolovaValue,
    isSelskRadaValue,
    isSelskStarostaValue,
    oblRada,
    rajonRada,
    pristine,
    reset,
    submitting
  } = props
  const oneIsChoosed = isProportionValue || isMajorValue || isOblRadaValue || isRajonValue || isMiskRadaValue
  return (
    <form onSubmit={handleSubmit} className='settings'>
      <h1 className='heading-1 mb-lg'>Налаштування даних ДВК</h1>
      <div className='settings__numbers mb-sm'>
        <label>№ виборчої дільниці</label>
        <Field
          className='settings__numbers-input'
          title='6ти значное число'
          name='voteDist'
          component={renderField}
          type='number'
          validate={equalLength6}
        />
      </div>
      <div className='settings__numbers mb-sm'>
        <label>Кількість членів дільничної виборчої комісії</label>
        <Field
          className='settings__numbers-input'
          name='members'
          title='от 4 до 18'
          component={renderField}
          type='number'
          validate={[minValue4, maxValue18]}
        />
      </div>
      <div className='settings__numbers mb-sm'>
        <label>Дата проведення виборів</label>
        <Field
          className='settings__numbers-input'
          name='voteDate'
          component='input'
          type='date'
          placeholder='Дата проведення виборів'
        />
      </div>
      <h2 className='heading-2 mb-md'>Типи виборів, які проводить ДВК</h2>
      <div className='settings__checkboxes mb-sm'>
        <Field
          name='isProportion'
          id='isProportion'
          component='input'
          type='checkbox'
        />
        <label htmlFor='isProportion'>
          Вибори народних депутатів України за пропорційною системою у
          загальнодержавному багатомандатному виборчому окрузі
        </label>
      </div>
      <div className='settings__checkboxes mb-sm'>
        <Field name='isMajor' id='isMajor' component='input' type='checkbox' />
        <label htmlFor='isMajor'>
          Вибори народних депутатів України за мажоритарною системою відносної
          більшості в одномандатних виборчих округах
        </label>
      </div>
      <div className='settings__checkboxes mb-sm'>
        <div>
          <Field
            name='isOblRada'
            id='isOblRada'
            component='input'
            type='checkbox'
          />
          <label htmlFor='isOblRada'>
            Вибори депутатів обласної ради (ВР АР Крим)
          </label>
          {isOblRadaValue && (
            <div className='settings__checkboxes-radio'>
              <label>
                <Field
                  name='oblRada'
                  component='input'
                  type='radio'
                  value='krim'
                />
                Верховної Ради Автономної Республіки Крим
              </label>
              <label>
                <Field
                  name='oblRada'
                  component='input'
                  type='radio'
                  value='oblRada'
                />
                обласної ради
              </label>
            </div>
          )}
        </div>
      </div>
      <div className='settings__checkboxes mb-sm'>
        <div>
          <Field
            name='isRajon'
            id='isRajon'
            component='input'
            type='checkbox'
          />
          <label htmlFor='isRajon'>
            Вибори депутатів районної (районної в місті) ради
          </label>
          {isRajonValue && (
            <div className='settings__checkboxes-radio'>
              <label>
                <Field
                  name='rajonRada'
                  component='input'
                  type='radio'
                  value='rajonRada'
                />
                районної ради
              </label>
              <label>
                <Field
                  name='rajonRada'
                  component='input'
                  type='radio'
                  value='mistRada'
                />
                районної в місті ради
              </label>
            </div>
          )}
        </div>
      </div>
      <div className='settings__checkboxes mb-sm'>
        <Field
          name='isMiskRada'
          id='isMiskRada'
          component='input'
          type='checkbox'
        />
        <label htmlFor='isMiskRada'>Вибори депутатів міської ради</label>
      </div>
      <div className='settings__checkboxes mb-sm'>
        <div>
          <Field
            name='isMiskGolova'
            id='isMiskGolova'
            component='input'
            type='checkbox'
          />
          <label htmlFor='isMiskGolova'>
            Вибори міського (селищного, сільського) голови
          </label>
          {isMiskGolovaValue && (
            <div className='settings__checkboxes-radio'>
              <label>
                <Field
                  name='miskGolova'
                  component='input'
                  type='radio'
                  value='miskGolova'
                />
                міського голови
              </label>
              <label>
                <Field
                  name='miskGolova'
                  component='input'
                  type='radio'
                  value='selishGolova'
                />
                селищного голови
              </label>
              <label>
                <Field
                  name='miskGolova'
                  component='input'
                  type='radio'
                  value='selskGolova'
                />
                сільського голови
              </label>
            </div>
          )}
        </div>
      </div>
      <div className='settings__checkboxes mb-sm'>
        <div>
          <Field
            name='isSelskRada'
            id='isSelskRada'
            component='input'
            type='checkbox'
          />
          <label htmlFor='isSelskRada'>
            Вибори депутатів сільської (селищної) ради
          </label>
          {isSelskRadaValue && (
            <div className='settings__checkboxes-radio'>
              <label>
                <Field
                  name='selskRada'
                  component='input'
                  type='radio'
                  value='selskRada'
                />
                сільської ради
              </label>
              <label>
                <Field
                  name='selskRada'
                  component='input'
                  type='radio'
                  value='selishRada'
                />
                селищної ради
              </label>
            </div>
          )}
        </div>
      </div>
      <div className='settings__checkboxes mb-sm'>
        <div>
          <Field
            name='isSelskStarosta'
            id='isSelskStarosta'
            component='input'
            type='checkbox'
          />
          <label htmlFor='isSelskStarosta'>
            Вибори сільського (селищного) старости
          </label>
          {isSelskStarostaValue && (
            <div className='settings__checkboxes-radio'>
              <label>
                <Field
                  name='selskStarosta'
                  component='input'
                  type='radio'
                  value='selskStarosta'
                />
                сільського старости
              </label>
              <label>
                <Field
                  name='selskStarosta'
                  component='input'
                  type='radio'
                  value='selishStarosta'
                />
                селищного старости
              </label>
            </div>
          )}
        </div>
      </div>
      {oneIsChoosed && <h2 className='heading-2 mb-md'>Номери округів для кожного з типів виборів</h2>}
      {isOblRadaValue && (
        <div className='settings__district mb-sm'>
          <label>
            № виборчого округа на виборах {oblRada === 'krim' ? 'Верховної Ради Автономної Республіки Крим' : 'обласної ради'}
          </label>
          <div>
            <Field
              name='numViborOkrugOblRada'
              component='input'
              type='number'
              className='settings__district-input'
            />
          </div>
        </div>
      )}
      {isMiskRadaValue && (
        <div className='settings__district mb-sm'>
          <label>№ виборчого округа на виборах депутатів міської ради</label>
          <div>
            <Field
              name='numViborOkrugMiskRada'
              component='input'
              type='number'
              className='settings__district-input'
            />
          </div>
        </div>
      )}
      {isRajonValue && (
        <div className='settings__district mb-sm'>
          <label>
            № виборчого округа на виборах {rajonRada === 'rajonRada' ? 'районної ради ' : 'районної в місті ради'}
          </label>
          <div>
            <Field
              name='numViborOkrugRajonRada'
              component='input'
              type='number'
              className='settings__district-input'
            />
          </div>
        </div>
      )}
      {(isProportionValue || isMajorValue) && (
        <div className='settings__district mb-sm'>
          <label>
            № виборчого округа на виборах народних депутатів України
          </label>
          <div>
            <Field
              name='numViborOkrugNarodDeput'
              component='input'
              type='number'
              className='settings__district-input'
            />
          </div>
        </div>
      )}
      {(isProportionValue || isMajorValue) && <BoxForm />}
      <div>
        <button
          type='button'
          className='btn'
          disabled={pristine || submitting}
          onClick={reset}
        >
          Очистити поля
        </button>
      </div>
    </form>
  )
}

// The order of the decoration does not matter.

// Decorate with redux-form
Settings = reduxForm({
  form: 'dvkForm', // a unique identifier for this form
  initialValues: {
    isProportion: false,
    isMajor: false,
    isRajon: false,
    isOblRada: false,
    oblRada: 'krim',
    rajonRada: 'rajonRada',
    miskGolova: 'miskGolova',
    selskRada: 'selskRada',
    selskStarosta: 'selskStarosta'
  },
  destroyOnUnmount: false
})(Settings)

// Decorate with connect to read form values
const selector = formValueSelector('dvkForm') // <-- same as form name
Settings = connect(state => {
  // can select values individually
  const isProportionValue = selector(state, 'isProportion')
  const isMajorValue = selector(state, 'isMajor')
  const isOblRadaValue = selector(state, 'isOblRada')
  const isRajonValue = selector(state, 'isRajon')
  const isMiskRadaValue = selector(state, 'isMiskRada')
  const isMiskGolovaValue = selector(state, 'isMiskGolova')
  const isSelskRadaValue = selector(state, 'isSelskRada')
  const isSelskStarostaValue = selector(state, 'isSelskStarosta')
  const oblRada = selector(state, 'oblRada')
  const rajonRada = selector(state, 'rajonRada')
  return {
    isProportionValue,
    isMajorValue,
    isRajonValue,
    isOblRadaValue,
    isMiskRadaValue,
    isMiskGolovaValue,
    isSelskRadaValue,
    isSelskStarostaValue,
    oblRada,
    rajonRada
  }
})(Settings)

Settings.propTypes = {
  handleSubmit: PropTypes.func,
  isMajorValue: PropTypes.bool,
  isOblRadaValue: PropTypes.bool,
  isProportionValue: PropTypes.bool,
  isRajonValue: PropTypes.bool,
  isMiskRadaValue: PropTypes.bool,
  isMiskGolovaValue: PropTypes.bool,
  isSelskRadaValue: PropTypes.bool,
  isSelskStarostaValue: PropTypes.bool,
  oblRada: PropTypes.string,
  rajonRada: PropTypes.string,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  reset: PropTypes.bool
}

export default Settings
