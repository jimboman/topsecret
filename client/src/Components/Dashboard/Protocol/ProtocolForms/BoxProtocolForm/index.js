import React, { Component } from 'react'
import { Field } from 'redux-form'
import PropTypes from 'prop-types'
import BoxTable from './BoxTable'
import CandidatesTable from 'Components/Dashboard/Protocol/ProtocolForms/CommonForm/CandidatesTable'
import './BoxProtocolForm.scss'

const renderField = ({
  input,
  type,
  title,
  label,
  smallLabel,
  className,
  meta: { touched, error, warning },
  ...props
}) => (
  <div className={className}>
    <label>
      {`${input.name.split('value')[1]}) ${label}`}
      <br />
      <small>{smallLabel}</small>
    </label>
    <input
      {...input}
      {...props}
      className='boxProtocolForm__numbers-input'
      type={type}
      title={title}
    />
    {touched &&
      ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
  </div>
)

export default class BoxProtocolForm extends Component {
  getSumOfBoxVotes = () => {
    return 2
  }

  getTotalVotes = () => {
    try {
      const votes = this.props.getCandidateVotes()
      return Object.values(votes).reduce((acc, curr) => Number(acc) + Number(curr))
    } catch (err) {
      console.log(err)
      return null
    }
  }

  render () {
    const { header, boxProtocolText, boxes, isChecked, candidates } = this.props
    return (
      <div className='boxProtocolForm'>
        <h2 className='heading-2 mb-md'>{header}</h2>
        <Field
          className='boxProtocolForm__numbers'
          label={boxProtocolText[1].label}
          smallLabel={boxProtocolText[1].smallLabel}
          name='value1'
          component={renderField}
          type='number'
        />
        <Field
          className='boxProtocolForm__numbers'
          label={boxProtocolText[2].label}
          smallLabel={boxProtocolText[2].smallLabel}
          name='value2'
          component={renderField}
          type='number'
        />
        <Field
          className='boxProtocolForm__numbers'
          label={boxProtocolText[3].label}
          smallLabel={boxProtocolText[3].smallLabel}
          name='value3'
          component={renderField}
          type='number'
        />
        <Field
          className='boxProtocolForm__numbers'
          label={boxProtocolText[4].label}
          smallLabel={boxProtocolText[4].smallLabel}
          name='value4'
          component={renderField}
          type='number'
        />
        <Field
          className='boxProtocolForm__numbers'
          label={boxProtocolText[5].label}
          smallLabel={boxProtocolText[5].smallLabel}
          name='value5'
          component={renderField}
          type='number'
        />
        <Field
          className='boxProtocolForm__numbers'
          label={boxProtocolText[6].label}
          smallLabel={boxProtocolText[6].smallLabel}
          name='value6'
          component={renderField}
          type='number'
        />
        <Field
          className='boxProtocolForm__numbers'
          label={boxProtocolText[7].label}
          smallLabel={boxProtocolText[7].smallLabel}
          name='value7'
          component={renderField}
          type='number'
          disabled
        />
        <BoxTable
          boxes={boxes}
          label={boxProtocolText[8].label}
          notCalculated={isChecked}
          total={this.getSumOfBoxVotes}
        />
        {/* <Field
          className='boxProtocolForm__numbers'
          label={boxProtocolText[8].label}
          smallLabel={boxProtocolText[8].smallLabel}
          name='value8'
          component={renderField}
          type='number'
        /> */}
        <Field
          className='boxProtocolForm__numbers'
          label={boxProtocolText[9].label}
          smallLabel={boxProtocolText[9].smallLabel}
          name='value9'
          component={renderField}
          type='number'
        />
        <Field
          className='boxProtocolForm__numbers'
          label={boxProtocolText[10].label}
          smallLabel={boxProtocolText[10].smallLabel}
          name='value10'
          component={renderField}
          type='number'
        />
        <CandidatesTable
          candidates={candidates}
          label11={`gggg`}
          label12={`wwwww`}
          secondColumnTitle='Назва місцевої організації партії'
          thirdColumnTitle='Кількість голосів виборців, поданих за місцеву організацію політичної партії'
          total={this.getTotalVotes()}
        />
      </div>
    )
  }
}

BoxProtocolForm.propTypes = {
  header: PropTypes.string.isRequired,
  boxProtocolText: PropTypes.object.isRequired,
  candidates: PropTypes.array,
  getCandidateVotes: PropTypes.func,
  isChecked: PropTypes.bool,
  boxes: PropTypes.array
}
