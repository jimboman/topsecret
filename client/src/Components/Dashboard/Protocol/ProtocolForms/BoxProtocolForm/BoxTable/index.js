import React, { Component } from 'react'
import { Field } from 'redux-form'
import './BoxTable.scss'
import PropTypes from 'prop-types'

const columnTitle = [
  'Тип скриньки',
  '№ скриньки',
  'Кількість бюлетенів',
  'Не підлягають врахуванню'
]

export default class BoxTable extends Component {
  render () {
    const {
      boxes,
      label,
      notCalculated,
      total
    } = this.props
    return (
      <div className='box'>
        <label>{`8) ${label}`}</label>
        <table className='box__table'>
          <thead>
            <tr>
              <th>{columnTitle[0]}</th>
              <th>{columnTitle[1]}</th>
              <th>{columnTitle[2]}</th>
              <th>{columnTitle[3]}</th>
            </tr>
          </thead>
          <tbody>
            {boxes.map((box, index) => (
              <tr key={index}>
                <td data-label={columnTitle[0]}>{box.type === 'stac' ? 'Стаціонарна' : 'Переносна'} скринька</td>
                <td data-label={columnTitle[1]}>
                  {box.num}
                </td>
                <td data-label={columnTitle[2]}>
                  <Field
                    name={`box-${index}`}
                    type='number'
                    disabled={notCalculated}
                    className='box__table-input'
                    component='input'
                  />
                </td>
                <td data-label={columnTitle[3]}>
                  <Field
                    name={`checkbox-${index}`}
                    type='checkbox'
                    className='box__table-checkbox'
                    component='input'
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <div className='box__numbers'>
          <label>Всього:</label>
          <label htmlFor=''>{total}</label>
        </div>
      </div>
    )
  }
}

BoxTable.defaultProps = {
  boxes: [],
  notCalculated: false,
  total: null
}

BoxTable.propTypes = {
  boxes: PropTypes.array,
  label: PropTypes.string.isRequired,
  notCalculated: PropTypes.bool,
  total: PropTypes.number
}
