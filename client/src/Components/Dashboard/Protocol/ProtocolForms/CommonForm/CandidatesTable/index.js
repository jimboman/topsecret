import React, { Component } from 'react'
import { Field } from 'redux-form'
import PropTypes from 'prop-types'
import './CandidatesTable.scss'

export default class CandidatesTable extends Component {
  static propTypes = {
    candidates: PropTypes.array,
    label11: PropTypes.string.isRequired,
    label12: PropTypes.string.isRequired,
    secondColumnTitle: PropTypes.string.isRequired,
    thirdColumnTitle: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    total: PropTypes.number
  }

  static defaultProps = {
    candidates: [],
    disabled: false,
    total: null
  }

  render () {
    const {
      candidates,
      label11,
      label12,
      secondColumnTitle,
      thirdColumnTitle,
      disabled,
      total
    } = this.props
    return (
      <div className='candidatesTable'>
        <label>{`11) ${label11}`}</label>
        <table className='candidatesTable__table'>
          <thead>
            <tr>
              <th className='firstCol'>№</th>
              <th className='secondCol'>{secondColumnTitle}</th>
              <th className='thirdCol'>{thirdColumnTitle}</th>
            </tr>
          </thead>
          <tbody>
            {candidates.map((candidate, index) => (
              <tr key={index}>
                <td data-label='№'>{index + 1}</td>
                <td data-label={secondColumnTitle}>{candidate}</td>
                <td data-label={thirdColumnTitle}>
                  <Field
                    name={`candidate-${index}`}
                    type='number'
                    disabled={disabled}
                    className='candidatesTable__numbers-input'
                    component='input'
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <div className='candidatesTable__numbers'>
          <label>{`12) ${label12}`}</label>
          <label htmlFor=''>{total}</label>
        </div>
      </div>
    )
  }
}
