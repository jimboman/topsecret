import React, { Component } from 'react'
import { Field, reduxForm, formValueSelector } from 'redux-form'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import CandidatesTable from './CandidatesTable'
import './CommonForm.scss'

const renderField = ({
  input,
  type,
  title,
  label,
  smallLabel,
  className,
  meta: { touched, error, warning },
  ...props
}) => (
  <div className={className}>
    <label>
      {`${input.name.split('value')[1]}) ${label}`}
      <br />
      <small>{smallLabel}</small>
    </label>
    <input
      {...input}
      {...props}
      className='commonProtocolForm__numbers-input'
      type={type}
      title={title}
    />
    {touched &&
      ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
  </div>
)

export class CommonProtocolForm extends Component {
  getTotalVotes = () => {
    try {
      const votes = this.props.getCandidateVotes()
      return Object.values(votes).reduce((acc, curr) => Number(acc) + Number(curr))
    } catch (err) {
      console.log(err)
      return null
    }
  }
  render () {
    const { header, commonProtocolText, candidates } = this.props
    return (
      <div className='commonProtocolForm'>
        <h2 className='heading-2 mb-md'>{header}</h2>
        <Field
          className='commonProtocolForm__numbers'
          label={commonProtocolText[1].label}
          smallLabel={commonProtocolText[1].smallLabel}
          name='value1'
          component={renderField}
          type='number'
        />
        <Field
          className='commonProtocolForm__numbers'
          label={commonProtocolText[2].label}
          smallLabel={commonProtocolText[2].smallLabel}
          name='value2'
          component={renderField}
          type='number'
        />
        <Field
          className='commonProtocolForm__numbers'
          label={commonProtocolText[3].label}
          smallLabel={commonProtocolText[3].smallLabel}
          name='value3'
          component={renderField}
          type='number'
        />
        <Field
          className='commonProtocolForm__numbers'
          label={commonProtocolText[4].label}
          smallLabel={commonProtocolText[4].smallLabel}
          name='value4'
          component={renderField}
          type='number'
        />
        <Field
          className='commonProtocolForm__numbers'
          label={commonProtocolText[5].label}
          smallLabel={commonProtocolText[5].smallLabel}
          name='value5'
          component={renderField}
          type='number'
        />
        <Field
          className='commonProtocolForm__numbers'
          label={commonProtocolText[6].label}
          smallLabel={commonProtocolText[6].smallLabel}
          name='value6'
          component={renderField}
          type='number'
        />
        <Field
          className='commonProtocolForm__numbers'
          label={commonProtocolText[7].label}
          smallLabel={commonProtocolText[7].smallLabel}
          name='value7'
          component={renderField}
          type='number'
          disabled
        />
        <Field
          className='commonProtocolForm__numbers'
          label={commonProtocolText[8].label}
          smallLabel={commonProtocolText[8].smallLabel}
          name='value8'
          component={renderField}
          type='number'
        />
        <Field
          className='commonProtocolForm__numbers'
          label={commonProtocolText[9].label}
          smallLabel={commonProtocolText[9].smallLabel}
          name='value9'
          component={renderField}
          type='number'
        />
        <Field
          className='commonProtocolForm__numbers'
          label={commonProtocolText[10].label}
          smallLabel={commonProtocolText[10].smallLabel}
          name='value10'
          component={renderField}
          type='number'
        />
        <CandidatesTable
          candidates={candidates}
          label11={commonProtocolText[11].label}
          label12={commonProtocolText[12].label}
          secondColumnTitle='Назва місцевої організації партії'
          thirdColumnTitle='Кількість голосів виборців, поданих за місцеву організацію політичної партії'
          total={this.getTotalVotes()}
        />
      </div>
    )
  }
}

CommonProtocolForm.propTypes = {
  header: PropTypes.string.isRequired,
  commonProtocolText: PropTypes.object.isRequired,
  candidates: PropTypes.array,
  getCandidateVotes: PropTypes.func
}

// export default reduxForm({ destroyOnUnmount: false })(CommonProtocolForm)

export const withFormName = (WrappedForm, formName) => {
  let FormWithName = props => <WrappedForm form={formName} {...props} />
  FormWithName = reduxForm({
    form: formName,
    destroyOnUnmount: false
  })(FormWithName)

  const selector = formValueSelector(formName)
  const mapStateToProps = state => ({
    valueByNum: num => selector(state, `value${num}`),
    candidates: state.form.names ? state.form.names.values[formName] : [],
    getCandidateVotes: () => state.form[formName].values,
    boxes: state.form.boxArrays ? state.form.boxArrays.values.box : []
  })
  FormWithName = connect(mapStateToProps)(FormWithName)
  return FormWithName
}
