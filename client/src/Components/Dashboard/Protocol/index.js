import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from '@reach/router'
import { connect } from 'react-redux'
import './Protocol.scss'
import { LINKNAMES } from './textToFroms'

class Protocol extends Component {
  renderMiskGolova = ({ miskGolova }) => (
    <Link to={`miskGolova/${miskGolova}`}>
      {LINKNAMES.miskGolova[`${miskGolova}`]}
    </Link>
  )

  renderRajonRada = ({ rajonRada }) => (
    <Link to={`rajon/${rajonRada}`}>{LINKNAMES.rajon[`${rajonRada}`]}</Link>
  )

  renderOblRada = ({ oblRada }) => (
    <Link to={`oblRada/${oblRada}`}>{LINKNAMES.oblRada[`${oblRada}`]}</Link>
  )

  renderSelskRada = ({ selskRada }) => (
    <Link to={`selskRada/${selskRada}`}>
      {LINKNAMES.selskRada[`${selskRada}`]}
    </Link>
  )

  renderSelskStarosta = ({ selskStarosta }) => (
    <Link to={`selskStarosta/${selskStarosta}`}>
      {LINKNAMES.selskStarosta[`${selskStarosta}`]}
    </Link>
  )

  render () {
    const { dvkFormValues } = this.props
    const {
      isProportion,
      isMajor,
      isMiskRada,
      isMiskGolova,
      isRajon,
      isOblRada,
      isSelskRada,
      isSelskStarosta
    } = dvkFormValues
    return (
      <div className='protocolList'>
        <h1 className='heading-1 mb-lg'>Протоколи про підрахунок голосів виборців</h1>
        {isProportion && <Link to='proportion'>{LINKNAMES.proportion}</Link>}
        {isMajor && <Link to='major'>{LINKNAMES.major}</Link>}
        {isRajon && this.renderRajonRada(dvkFormValues)}
        {isOblRada && this.renderOblRada(dvkFormValues)}
        {isMiskRada && <Link to='miskRada'>{LINKNAMES.miskRada}</Link>}
        {isMiskGolova && this.renderMiskGolova(dvkFormValues)}
        {isSelskRada && this.renderSelskRada(dvkFormValues)}
        {isSelskStarosta && this.renderSelskStarosta(dvkFormValues)}
      </div>
    )
  }
}

Protocol.propTypes = {
  dvkFormValues: PropTypes.object
}

const mapStateToProps = state => {
  return { dvkFormValues: state.form.dvkForm ? state.form.dvkForm.values : {} }
}

export default connect(mapStateToProps)(Protocol)
