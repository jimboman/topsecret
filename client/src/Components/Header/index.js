import React from 'react'
import PropTypes from 'prop-types'
import './Header.scss'
import svgSprite from 'img/sprite.svg'
import defaultAvatar from 'img/user.png'
import logo from 'img/logo.png'

const Header = (props) => (
  <header className='header'>
    <img src={logo} alt='voting logo' className='logo' />

    <form action='#' className='search'>
      <input type='text' className='search__input' placeholder='Search field' />
      <button className='search__button'>
        <svg className='search__icon'>
          <use xlinkHref={`${svgSprite}#icon-circle-with-cross`} />
        </svg>
      </button>
    </form>

    <nav className='user-nav'>
      <div className='user-nav__user'>
        <img src={props.user.avatar} alt='user-avatar' className='user-nav__user-photo' />
        <span className='user-nav__user-name'>
          {`${props.user.firstName} ${props.user.lastName}`}
        </span>
      </div>
    </nav>
  </header>
)

Header.propTypes = {
  user: PropTypes.shape({
    avatar: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired
  })
}

Header.defaultProps = {
  user: {
    avatar: defaultAvatar,
    firstName: 'Oleg',
    lastName: 'Kupriianov'
  }
}

export default Header
