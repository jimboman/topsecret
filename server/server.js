const express = require('express')
const app = express()
const port = process.env.PORT || 3001

app.use(express.json({limit: '50mb'}))
app.use(express.urlencoded({extended: true, limit: '50mb'}))

app.listen(port, () => console.log(`Listening on port  ${port}`))
